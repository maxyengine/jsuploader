const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const extractCSS = new ExtractTextPlugin('client/nrg-uploader.css')

module.exports = {
  entry: {
    'nrg-uploader': './src/index.js',
    'nrg-uploader-usage': './src/usage.js'
  },
  output: {
    filename: 'client/[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      title: 'Uploader',
      favicon: 'src/assets/favicon.ico'
    }),
    extractCSS
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: extractCSS.extract([
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: 'nrg__[name]__[local]___[hash:base64:5]',
              importLoaders: 1
            }
          },
          'postcss-loader',
          'sass-loader'
        ])
      },
      {
        test: /\.js$/,
        use: 'babel-loader'
      },
      {
        test: /\.html$/,
        use: ['babel-loader', '@nrg/html-loader']
      },
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?mimetype=image/svg+xml'},
      {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?mimetype=application/font-woff'},
      {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?mimetype=application/font-woff'},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?mimetype=application/octet-stream'},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader'}
    ]
  }
}