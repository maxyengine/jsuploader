<?php

namespace Nrg\Uploader\Service;

use JsonSerializable;
use Nrg\Utility\Abstraction\Config;
use Nrg\Utility\Abstraction\Settings;
use Nrg\Utility\Service\ArrayConfig;
use Nrg\Utility\Value\Size;
use RuntimeException;

/**
 * Class AppConfig
 */
class AppConfig extends ArrayConfig implements JsonSerializable
{
    /**
     * @var string
     */
    private $basePath;

    /**
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $config = $settings->getConfig(Config::class);
        $path = $config->get('path');
        $publicKeys = $config->get('publicKeys', []);
        $data = json_decode(file_get_contents($path), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new RuntimeException('Invalid config file format.');
        }

        $this->basePath = dirname($path);
        $this->resolve($data);

        parent::__construct($data, $publicKeys);
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        $data = parent::jsonSerialize();

        if ($this->has('maxSize')) {
            $data['maxSize'] = $this->get('maxSize')->getValue();
        }

        return $data;
    }

    private function resolve(array &$data)
    {
        $this
            ->resolveUploadsFolder($data)
            ->resolveMaxSize($data);
    }

    private function resolveUploadsFolder(array &$data): AppConfig
    {
        if (!empty($data['uploadsFolder'])) {
            $data['uploadsFolder'] = realpath("{$this->basePath}/{$data['uploadsFolder']}");
        }

        return $this;
    }


    private function resolveMaxSize(array &$data): AppConfig
    {
        if (!empty($data['maxSize'])) {
            $data['maxSize'] = Size::fromHumanString($data['maxSize']);
        }

        return $this;
    }
}
