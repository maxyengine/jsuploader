<?php

namespace Nrg\Uploader\Action;

use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Utility\Abstraction\Config;

/**
 * Class OpenAction.
 *
 * Opens a file by a path.
 */
class OpenAction
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $uploadsFolder;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->uploadsFolder = $config->get('uploadsFolder');
    }

    /**
     * Opens a file by a path.
     *
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $fileName = $event->getRequest()->getQueryParam('fileName');
        $this->path = $this->uploadsFolder.DIRECTORY_SEPARATOR.$fileName;

        if (!is_file($this->path)) {
            // todo: throw UrlNotFound if file is not exist (handler should be placed as observer)
        }

        $event->getResponse()->setHeader('Content-Type', mime_content_type($this->path).';charset=utf-8');
    }

    public function onComplete()
    {
        if (ob_get_level()) {
            ob_end_clean();
        }

        readfile($this->path);
    }
}
