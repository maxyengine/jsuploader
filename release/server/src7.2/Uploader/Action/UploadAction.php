<?php

namespace Nrg\Uploader\Action;

use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Uploader\Form\UploadFileForm;
use Nrg\Utility\Abstraction\Config;

/**
 * Class UploadAction.
 *
 * Uploads a file to a path.
 */
class UploadAction
{
    /**
     * @var string
     */
    private $uploadsFolder;

    /**
     * @var UploadFileForm
     */
    private $form;

    /**
     * @param Config $config
     */
    public function __construct(UploadFileForm $form, Config $config)
    {
        $this->uploadsFolder = $config->get('uploadsFolder');
        $this->form = $form;
    }

    /**
     * Uploads a file to a path.
     *
     * @param HttpExchangeEvent $event
     */
    public function onNext($event)
    {
        $this->form->populate($event->getRequest()->getUploadedFiles());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY))
                ->setBody($this->form->serialize());
        } else {
            $uploadedFile = $this->form->getElement('file')->getValue();
            $uploadedFile->moveTo($this->uploadsFolder.DIRECTORY_SEPARATOR.$uploadedFile->getName());
            $event->getResponse()->setStatusCode(HttpStatus::NO_CONTENT);
        }
    }
}
