<?php

namespace Nrg\Http\Exception;

use Exception;
use JsonSerializable;
use Nrg\Http\Value\HttpStatus;

/**
 * Class HttpException
 */
class HttpException extends Exception implements JsonSerializable
{
    /**
     * @var HttpStatus
     */
    private $status;

    public function __construct(...$args)
    {
        parent::__construct(...$args);
        $this->status = new HttpStatus($this->getCode(), $this->getMessage());
    }

    /**
     * @return HttpStatus
     */
    public function getStatus(): HttpStatus
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'body' => $this->getMessage(),
        ];
    }
}