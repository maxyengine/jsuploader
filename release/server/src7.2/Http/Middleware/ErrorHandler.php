<?php

namespace Nrg\Http\Middleware;

use DomainException;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Exception\HttpException;
use Nrg\Http\Value\ErrorMessage;
use Nrg\Http\Value\HttpStatus;
use Throwable;

/**
 * Class ErrorHandler
 */
class ErrorHandler
{
    /**
     * @param Throwable $throwable
     * @param HttpExchangeEvent $event
     */
    public function onError(Throwable $throwable, HttpExchangeEvent $event)
    {
        if ($throwable instanceof HttpException) {
            $event->getResponse()->setStatus($throwable->getStatus());
        } elseif ($throwable instanceof DomainException) {
            $event->getResponse()->setStatusCode(HttpStatus::BAD_REQUEST);
        } else {
            $event->getResponse()->setStatusCode(HttpStatus::INTERNAL_SERVER_ERROR);
        }

        $event->getResponse()->setBody(new ErrorMessage($throwable));
    }
}
