<?php

namespace Nrg\Http\Middleware;

use Nrg\Http\Abstraction\ResponseEmitter;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Nrg\Utility\Abstraction\Config;
use Nrg\Utility\Abstraction\Settings;

/**
 * Class AllowCors
 */
class AllowCors
{
    private const DEFAULT_MAX_AGE = 86400;
    private const DEFAULT_METHODS = ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'];

    /**
     * @var ResponseEmitter
     */
    private $emitResponse;

    /**
     * @var Config
     */
    private $config;


    public function __construct(ResponseEmitter $emitResponse, Settings $settings)
    {
        $this->emitResponse = $emitResponse;
        $this->config = $settings->getConfig(static::class);
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext(HttpExchangeEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            $response
                ->setHeader('Access-Control-Allow-Origin', $_SERVER['HTTP_ORIGIN'])
                ->setHeader('Access-Control-Allow-Credentials', 'true')
                ->setHeader('Access-Control-Max-Age', $this->config->get('maxAge', self::DEFAULT_MAX_AGE));
        }

        if ('OPTIONS' === $request->getMethod()) {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                $response->setHeader(
                    'Access-Control-Allow-Methods',
                    implode(',', $this->config->get('methods', self::DEFAULT_METHODS))
                );
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                $response->setHeader('Access-Control-Allow-Headers', $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
            }

            $response->setStatusCode(HttpStatus::OK);
            $this->emitResponse->emit($response, true);
        }
    }
}
