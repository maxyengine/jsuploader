<?php

use Nrg\Http\Abstraction\ResponseEmitter;
use Nrg\Http\Abstraction\RouteProvider;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Middleware\AllowCors;
use Nrg\Http\Middleware\ErrorHandler;
use Nrg\Http\Middleware\ParseJsonRequest;
use Nrg\Http\Middleware\SerializeJsonResponse;
use Nrg\Http\Middleware\OffPrettyUrls;
use Nrg\Http\Middleware\EmitResponse;
use Nrg\Http\Middleware\RunAction;
use Nrg\Http\Service\HttpResponseEmitter;
use Nrg\Http\Service\HttpRouteProvider;
use Nrg\I18n\Abstraction\Translator;
use Nrg\I18n\Service\I18nTranslator;
use Nrg\Rx\Abstraction\EventProvider;
use Nrg\Rx\Service\RxEventProvider;
use Nrg\Uploader\Action\ConfigAction;
use Nrg\Uploader\Action\OpenAction;
use Nrg\Uploader\Action\UploadAction;
use Nrg\Uploader\Service\AppConfig;
use Nrg\Utility\Abstraction\Config;

return [
    'routes' => [
        '/config' => ConfigAction::class,
        '/upload' => UploadAction::class,
        '/open' => OpenAction::class,
    ],
    'events' => [
        HttpExchangeEvent::class => [
            ErrorHandler::class,
            AllowCors::class,
            OffPrettyUrls::class,
            ParseJsonRequest::class,
            RunAction::class,
            SerializeJsonResponse::class,
            EmitResponse::class,
        ],
    ],
    'services' => [
        Config::class => AppConfig::class,
        EventProvider::class => RxEventProvider::class,
        RouteProvider::class => HttpRouteProvider::class,
        ResponseEmitter::class => HttpResponseEmitter::class,
        Translator::class => I18nTranslator::class,
    ],
    'config' => [
        Config::class => [
            'path' => realpath(__DIR__.'/../config.json'),
            'publicKeys' => [
                'maxSize',
                'denyExtensions',
                'allowExtensions',
            ],
        ],
    ],
];
