<?php

if (!empty($_GET['r'])) {
    require __DIR__.'/server/run.php';
    exit(0);
}

?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Uploader</title>
	<link href="client/nrg-uploader.css" rel="stylesheet">
</head>
<body>
<script type="text/javascript" src="client/nrg-uploader.js"></script>
<script type="text/javascript" src="client/nrg-uploader-usage.js"></script>
</body>
</html>