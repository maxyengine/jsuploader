var uploader = new $nrg.Uploader()

uploader.run().then(() => {
  uploader.appendTo(document.body)

  uploader.widget.trigger('selectFiles', {
    files: [
      {name: 'Electro Guitar 778.jpg', size: 1090},
      {name: 'Electro Guitar 778.pdf', size: 1090},
      {name: 'Electro Guitar 778.mp3', size: 1090}
    ]
  })
})

