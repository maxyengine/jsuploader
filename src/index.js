import 'babel-polyfill'
import 'classlist-polyfill'
import 'whatwg-fetch'

import './assets/icons.scss'
import './assets/theme/cyan.scss'

import { Injector, Value } from '@nrg/core'
import { Endpoint } from '@nrg/http'

import Uploader from './widgets/Uploader'
import Config from './services/Config'
import Client from './services/Client'
import UploaderFactory from './services/UploaderFactory'

window.$nrg = window.$nrg || {}
var $nrg = window.$nrg

$nrg.Uploader = $nrg.Uploader || class extends Value {

  get defaults () {
    return {
      apiUrl: '.',
      listHeight: 'auto'
    }
  }

  get services () {
    return {
      endpoint: [Endpoint, {apiUrl: this.apiUrl}],
      client: Client,
      uploaderFactory: UploaderFactory
    }
  }

  initialize () {
    this.injector = new Injector().loadServices(this.services)
    this.widget = this.injector.createObject(Uploader, {listHeight: this.listHeight})
  }

  run () {
    return this.injector.getService('client')
      .fetchConfig()
      .then(json => {
        this.injector.setService('config', [Config, json])

        if (this.wrapper) {
          this.appendTo(this.wrapper)
        }
      })
  }

  appendTo (wrapper) {
    wrapper.appendChild(this.widget.element)
  }
}