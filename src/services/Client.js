import { Value } from '@nrg/core'

const createUrl = Symbol()

export default class extends Value {

  static get services () {
    return {
      endpoint: 'endpoint',
      uploaderFactory: 'uploaderFactory'
    }
  }

  fetchConfig () {
    return fetch(this[createUrl]('/config'))
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok')
        }

        return response.json()
      })
      .catch(error => {
        console.error(error)
      })
  }

  createUploader () {
    return this.uploaderFactory.create({
      url: this[createUrl]('/upload')
    })
  }

  openFile (fileName) {
    window.open(this[createUrl]('/open', {fileName}), '_blank')
  }

  [createUrl] (route, queryParams = {}) {
    return this.endpoint.createUrl(route, queryParams)
  }
}