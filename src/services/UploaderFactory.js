import { Value } from '@nrg/core'
import Uploader from './Uploader'

export default class extends Value {

  create (properties = {}) {
    return this.injector.createObject(Uploader, properties)
  }
}