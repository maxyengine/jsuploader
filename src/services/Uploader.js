import { Component } from '@nrg/core'

export default class extends Component {

  upload (file) {
    const http = new XMLHttpRequest()
    const body = new FormData()

    body.append('file', file)

    http.upload.onprogress = (...args) => this.trigger('progress', ...args)
    http.onload = (...args) => this.trigger('load', ...args)
    http.onerror = (...args) => this.trigger('error', ...args)
    http.open('POST', this.url, true)
    http.send(body)

    this.http = http
  }

  abort () {
    this.http.abort()
  }
}