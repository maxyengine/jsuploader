import DragArea from './DragArea.html'

export default class extends DragArea {

  onDragStart (e) {
    e.preventDefault()
    this.highlight = true
  }

  onDragFinish (e) {
    e.preventDefault()
    this.highlight = false
  }

  onBrowse () {
    this.fileInput.click()
  }

  onDrop (event) {
    event.preventDefault()
    this.trigger('selectFiles', {files: event.dataTransfer.files})
  }

  onChangeFileInput (event) {
    this.trigger('selectFiles', {files: event.target.files})
    event.target.value = null
  }
}