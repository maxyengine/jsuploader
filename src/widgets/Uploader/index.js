import Uploader from './Uploader.html'

export default class extends Uploader {

  get properties () {
    return {
      listHeight: 'uploadList.listHeight'
    }
  }

  onSelectFiles ({files}) {
    this.uploadList.addFiles(files)
  }
}