import { Observer } from '@nrg/core'

export default class extends Observer {

  get element () {
    return this.owner.listWrapper
  }

  get listHeight () {
    return this.owner.listHeight
  }

  initialize () {
    super.initialize()

    window.addEventListener('resize', () => {
      this.alignHeight()
    })
  }

  onAddItem() {
    this.alignHeight()
  }

  onRemoveItem() {
    this.alignHeight()
  }

  alignHeight () {
    switch (this.listHeight) {
      case 'auto':
        const height = document.documentElement.clientHeight - this.element.offsetTop - 20
        this.element.style.height = height + 'px'
        break;
      default:
        if (this.listHeight) {
          this.element.style.height = this.listHeight
        }
    }
  }
}
