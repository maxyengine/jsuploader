import UploadList from './UploadList.html'
import UploadItem from '../UploadItem'
import AlignHeight from './AlignHeight'

const items = Symbol()

export default class extends UploadList {

  get traits () {
    return [
      AlignHeight
    ]
  }

  initialize () {
    this[items] = new Set()
  }

  addFiles (files) {
    for (const file of files) {
      this.addItem(this.createItem()).upload(file)
    }
  }

  createItem () {
    return this.injector.createObject(UploadItem).on(this)
  }

  addItem (item) {
    this.list.insertBefore(item.element, this.list.firstChild)
    this.trigger('addItem', {item})

    return item
  }

  removeItem (item) {
    this.list.removeChild(item.element)
    this.trigger('removeItem', {item})
  }

  onAddItem ({item}) {
    this.state = 'progress'
    this.total++
    this[items].add(item)
  }

  onLoad (event, item) {
    this.loaded++
  }

  onRemoveItem ({item}) {
    this.total--
    if ('load' === item.state) {
      this.loaded--
    }
    this[items].delete(item)

    if (!this[items].size) {
      this.state = 'empty'
    }
  }

  onClose (event, item) {
    this.removeItem(item)
  }

  onClear () {
    this[items].forEach(item => {
      item.close()
      this.removeItem(item)
    })
  }
}