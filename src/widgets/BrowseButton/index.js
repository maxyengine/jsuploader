import BrowseButton from './BrowseButton.html'

export default class extends BrowseButton {

  onClick () {
    this.fileInput.click()
  }
}