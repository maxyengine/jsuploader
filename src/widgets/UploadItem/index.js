import fileSize from 'filesize'
import fileExtension from 'file-extension'

import style from './UploadItem.scss'

import UploadItem from './UploadItem.html'
import FileExtensionValidator from './FileExtensionValidator'
import FileSizeValidator from './FileSizeValidator'

const total = Symbol()
const extension = Symbol()

export default class extends UploadItem {

  static get services () {
    return {
      client: 'client',
      config: 'config'
    }
  }

  get events () {
    return {
      load: {uploader: 'load'},
      progress: {uploader: 'progress'}
    }
  }

  initialize () {
    this.uploader = this.client.createUploader()
    this
      .addValidator(new FileExtensionValidator(this.config))
      .addValidator(new FileSizeValidator(this.config))
  }

  upload (file) {
    this.name = file.name
    this.total = file.size
    this[extension] = fileExtension(file.name)
    this.value = file

    if (this.hasError) {
      this.state = 'invalid'
    } else {
      this.state = 'progress'
      this.uploader.upload(file)
    }
  }

  close () {
    if ('progress' === this.state) {
      this.uploader.abort()
    }
  }

  onProgress ({loaded}) {
    this.loaded = loaded
  }

  onLoad () {
    this.state = 'load'
    this.fileIcon.classList.add(style[this[extension]])
  }

  onClose () {
    this.close()
  }

  onOpen () {
    this.client.openFile(this.name)
  }

  onBeforeSetTotal (event) {
    this[total] = event.value
    event.value = fileSize(event.value)
  }

  onBeforeSetLoaded (event) {
    if (event.value <= this[total]) {
      this.percent = Math.ceil(100 * event.value / this[total])
      event.value = fileSize(event.value)
    }
  }
}