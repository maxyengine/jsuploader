import ProgressBar from './ProgressBar.html'

export default class extends ProgressBar {

  onSetPercent ({value}) {
    this.element.setAttribute('data-value', value)
  }
}